# -*- coding:utf-8 -*-
import urllib
import urllib2
import json
import hashlib


class Api(object):
    __API_URL = u'https://controller-test.ecomtec.ru/'
    __USER_REDIRECT_PAGE = u'https://payform-test.ecomtec.ru'
    __secret_key = None
    _debug = None

    CMD_PREPARE_ORDER = u'prepare_order'
    CMD_CUSTOM_OPERATION = u'trans'

    PARAM_ACCOUNT_KEY = u'account_key'
    PARAM_SCHEMA_ID = u'schema_id'
    PARAM_ORDER_ID = u'order_id'
    PARAM_CLIENT_DATA = u'client_data'
    PARAM_CURRENCY = u'currency'
    PARAM_PRICE = u'price'
    PARAM_SIGN = u'sign'
    PARAM_FORM_DATA = u'form_data'
    PARAM_ID = u'id'
    PARAM_STATUS = u'status'
    PARAM_RETURN_URL = u'return_url'
    PARAM_CALLBACK_URL = u'callback_url'

    #Одностадийная операция списания средств по карте
    STATUS_ONE_STEP_FUND = 1
    #Предавторизация средств по карте
    STATUS_PREAUTHORIZATION = 3
    #Авторизация средств по карте
    STATUS_AUTHORIZATION = 4
    #Рекуррентный платеж по карте
    STATUS_RECURRENT_PAYMENT = 5
    #Отмена предавторизации по карте
    STATUS_PREAUTHORIZATION_CANCELLATION = 6
    #Возврат средств по карте
    STATUS_REFUND = 7

    __params = dict()

    def __init__(self, account_key, secret_key, debug=False, api_url=None):
        self.__secret_key = secret_key
        self.__account_key = account_key
        self._debug = debug
        if api_url is not None:
            self.__API_URL = api_url

    def __flush_params(self):
        self.__params = dict()

    def __set_param(self, key, value):
        self.__params[key] = value

    def __get_params(self):
        """
        Возвращает словарь параметров, секретный ключ включается в параметры
        :return:
        """
        params = self.__params.copy()
        params.update({self.PARAM_ACCOUNT_KEY: self.__account_key})
        params_encoded = dict()
        for key, value in params.iteritems():
            if isinstance(value, unicode):
                value = value.encode(u'utf8')
            params_encoded[key] = value
        return params_encoded

    def __exec_cmd(self, cmd):
        url = self.__API_URL
        params = dict(action=cmd)
        for key, value in self.__get_params().iteritems():
            params[u'data[%s]' % key] = value
        data = urllib.urlencode(params)
        request = urllib2.Request(url, data=data)
        opener = urllib2.build_opener(urllib2.HTTPSHandler(debuglevel=1))
        self.__flush_params()
        response = opener.open(request)
        response = json.loads(response.read())
        return response

    def __set_schema_id(self, schema_id):
        self.__set_param(self.PARAM_SCHEMA_ID, schema_id)

    def __set_order_id(self, order_id):
        self.__set_param(self.PARAM_ORDER_ID, order_id)

    def __set_client_data(self, client_data):
        self.__set_param(self.PARAM_CLIENT_DATA, client_data)

    def __set_currency(self, currency):
        self.__set_param(self.PARAM_CURRENCY, currency)

    def __set_price(self, price):
        self.__set_param(self.PARAM_PRICE, price)

    def __set_sign(self):
        sign = hashlib.sha1(self.__account_key + u'|' + self.__secret_key).hexdigest()
        self.__set_param(self.PARAM_SIGN, sign)

    def __set_form_data(self, form_data):
        self.__set_param(self.PARAM_FORM_DATA, form_data)

    def __set_id(self, id):
        self.__set_param(self.PARAM_ID, id)

    def __set_callback_url(self, callback_url):
        self.__set_param(self.PARAM_CALLBACK_URL, callback_url)

    def __set_return_url(self, return_url):
        self.__set_param(self.PARAM_RETURN_URL, return_url)

    def __set_status(self, status):
        self.__set_param(self.PARAM_STATUS, status)

    def prepare_order(self, order_id, client_data, price, form_data, callback_url, return_url=None, schema_id=1,
                      currency=u'RUB'):
        """
        Регистрация заказа в системе процессинга
        :param order_id:идентификатор заказа внутри системы мерчанта
        :param client_data:строка для сквозной передачи данных через процессинг (вы получите ее на колбеке)
        :param currency:валюта операции по умолчанию RUB
        :param price:сумма операции c разделителем «точка»
        :param sign:подпись
        :param form_data:массив вида key->value, служит для отображения произвольных данных пользователю на платежной
        странице, где key и value произвольные строки и служат для информирования пользователя мерчантом о заказе
        :return:
        """
        self.__set_order_id(order_id)
        self.__set_client_data(client_data)
        self.__set_currency(currency)
        self.__set_price(price)
        self.__set_sign()
        self.__set_form_data(form_data)
        self.__set_schema_id(schema_id)
        self.__set_callback_url(callback_url)
        self.__set_return_url(return_url)
        
        response = self.__exec_cmd(self.CMD_PREPARE_ORDER)
        if response.get(u'result', u'') == u'success':
            token = response[u'data'][u'token']
            return self.__USER_REDIRECT_PAGE + u'?token=' + token
        else:
            return None

    def custom_operation(self, id, sign, price, status):
        """
        Мерчанту из его системы доступна возможность проведения ряда дополнительных операций: возврат средств,
        рекуррентное списание, отмена предавторизации, а также подтверждение предавторизованной транзакции.
        Эти операции выполняются без необходимости передачи карточных или иных приватных данных,
        а на основе полученного идентификатора «первичной» транзакции.
        :param id:идентификатор «первичной» транзакции в системе процессинга
        :param sign:подпись
        :param price:сумма операции c разделителем «точка»
        :param status:тип транзакции
        :return:
        """
        self.__set_id(id)
        self.__set_sign(sign)
        self.__set_price(price)
        self.__set_status(status)
        response = self.__exec_cmd(self.CMD_CUSTOM_OPERATION)
        if response.get(u'result', u'') == u'success':
            return response[u'data'][u'token']
        else:
            return None
